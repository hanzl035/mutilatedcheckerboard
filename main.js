let board = []
const boardnode = document.querySelector("svg")
// converts r or c into the units of the svg's dimensions
function convert(i) {
 return String(-60 + 30 * Number(i))
}
function check(r, c) {
 if (
  board.some(piece => piece.covers(r, c))
 ) throw new Error("r,c already covered")
}
// removes piece from board with corresponding node
function remove(node) {
 board = board.filter(piece => piece.node != node)
}

class Block {
 static template = document.querySelector("#block-template")
 static clone(r, c) {
  const svg = this.template.content.cloneNode(true).querySelector("svg")
  svg.setAttribute("x", convert(c))
  svg.setAttribute("y", convert(r))
  return svg
 }
 constructor(r, c) {
  check(r, c)
  this.r = r
  this.c = c
  this.node = Block.clone(r, c)
 }
 covers(r, c) {
  return (r == this.r && c == this.c)
 }
}

class Mark {
 static template = document.querySelector("#mark-template")
 static clone(r, c) {
  const svg = this.template.content.cloneNode(true).querySelector("svg")
  svg.setAttribute("x", convert(c))
  svg.setAttribute("y", convert(r))
  return svg
 }
 constructor(r, c) {
  check(r, c)
  this.r = r
  this.c = c
  this.node = Mark.clone(r, c)
 }
 covers(r, c) {
  return (r == this.r && c == this.c)
 }
}

class Dominoe {
 static template = document.querySelector("#dominoe-template")
 static clone(r, c, direction) {
  const svg = this.template.content.cloneNode(true).querySelector("svg")
  svg.setAttribute("x", convert(c))
  svg.setAttribute("y", convert(r))
  const flipper = svg.querySelector(".flipper")
  flipper.classList.add(direction)
  return svg
 }
 constructor(mark, r, c) {
  if (!(mark.r == r ^ mark.c == c)) {
   throw new Error("one i must match, one must differ")
  }
  if (Math.abs(mark.r - r) > 1) {
   throw new Error("incompatible r's")
  }
  if (Math.abs(mark.c - c) > 1) {
   throw new Error("incompatible c's")
  }
  let direction
  if (mark.c != c) {
   if (mark.c < c) {direction = "right"}
   else {direction = "left"}
  }
  else /*(mark.r != r)*/ {
   if (mark.r < r) {direction = "down"}
   else {direction = "up"}
  }
  check(r, c)
  this.r1 = mark.r
  this.c1 = mark.c
  this.r2 = r
  this.c2 = c
  this.node = Dominoe.clone(this.r1, this.c1, direction)
 }
 covers(r, c) {
  return (
   (r == this.r1 && c == this.c1) ||
   (r == this.r2 && c == this.c2)
  )
 }
}

const svg = document.querySelector("svg")
const handler = {}
const block1 = new Block("1","1")
const block2 = new Block("8","8")
board.push(block1, block2)
svg.appendChild(block1.node)
svg.appendChild(block2.node)
function mutator(mark) {
 return (e) => {
  if (e.target.classList.contains("tile")) {
   const r = e.target.dataset.r
   const c = e.target.dataset.c
   try {
    const dominoe = new Dominoe(mark, r, c)
    board.push(dominoe)
    boardnode.appendChild(dominoe.node)
   }
   catch (e) {
    //console.log(e)
   }
   finally {
    boardnode.removeChild(mark.node)
    handler.handleEvent = handleAddRemove
   }
  }
  else {
   boardnode.removeChild(mark.node)
   handler.handleEvent = handleAddRemove
  }
 }
}

function handleAddRemove(e) {
 let target = e.target
 // assume the parentNode is always boards svg
 if (target.classList.contains("tile")) {
  const r = target.dataset.r
  const c = target.dataset.c
  try {
   const mark = new Mark(r, c)
   boardnode.appendChild(mark.node)
   handler.handleEvent = mutator(mark)
  }
  catch (e) {
   //console.log(e)
  }
 }
 else {
  while (target.tagName != "svg") {
   target = target.parentNode
  }
  if (target.classList.contains("dominoe")) {
   remove(target)
   boardnode.removeChild(target)
  }
 }
}
handler.handleEvent = handleAddRemove

svg.addEventListener("click", handler)


